'use strict';

// Declare app level module which depends on views, and components
angular.module('fxMockApp', [
  'fxMockApp.pricing',
  'fxMockApp.booking',
]).
config(['$routeProvider', function ($routeProvider) {
    $routeProvider.otherwise({
        templateUrl: 'pages/pricing.html',
    });
}]);