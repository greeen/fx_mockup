 var module = angular.module('fxMockApp');


 module.service('pricingService', function () {
     this.users = ['John', 'James', 'Jake'];

     this.pricingDetail = null;

     this.getPricingDetail = function () {
         return this.pricingDetail;
     };

     this.setPricingDetail = function (obj) {
         this.pricingDetail = obj;
     };
 });