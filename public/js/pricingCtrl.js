// Each controller and module combine together, to make easier understanding
var pricingModule = angular.module('fxMockApp.pricing', ['ngMessages']);


pricingModule.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/pricing', {
        templateUrl: 'pages/pricing.html',
        // required when various moduel navigation like menu items
    });
}]);



pricingModule.controller('pricingController', function ($scope, $location, commonFactory, pricingService) {

    // above input fields
    $scope.selectCurrencyPair = '';
    $scope.inputPrice = '0.0';
    $scope.selectStyle = '-1';
    $scope.selectOptionType = '';
    $scope.txtVolatility = '0';
    $scope.txtIntrestRate = '0';
    $scope.isPricingCalculated = true;


    //for pricing fields
    $scope.pricing = new Object();
    $scope.pricing.txtOptionType = 'CALL / PUT';
    $scope.pricing.txtSymbol = '';
    $scope.pricing.txtOptionValue = '';
    $scope.pricing.txtDelta = '';
    $scope.pricing.txtGamma = '';
    $scope.pricing.txtCurrencyPair = '';
    $scope.pricing.isCalculationDone = false;
    //$scope.pricing.optionValue='';


    $scope.calculatePricing = function () {

        $scope.isPricingCalculated = false;

        bootbox.confirm({
            size: 'small',
            title: "Message",
            message: "Are you sure to check the price ?",
            callback: function (result) {
                // console.log(result + ' - ' + $scope.selectOptionType 
                $scope.calculate();


                console.log('calculation done');
            }
        });

    };

    $scope.proceedBooking = function () {
        $scope.setPricingDetail();
        $location.url('/booking');
        $('.active').removeClass('active active_bold');
        $('#listBooking').addClass('active active_bold');
    };

    $scope.setPricingDetail = function () {
        console.log("pricing value : " + JSON.stringify($scope.pricing));
        pricingService.setPricingDetail($scope.pricing);
    };


    $scope.calculate = function () {
        //for demo purpose values were hardcoded here
        console.log('Calculating Value');

        $('#txtOptionType').val($scope.selectOptionType);
        $scope.pricing.txtOptionType = $scope.selectOptionType;
        $scope.pricing.txtCurrencyPair = $scope.selectCurrencyPair
        $scope.pricing.isCalculationDone = true;
        $('#bttnBook').addClass('btn btn-forex');


        var testCase = $scope.selectCurrencyPair;

        switch (testCase) {
        case 'USD_SGD':
            $('#txtOptionValue').val('1.348');
            $('#txtDelta').val('0.231');
            $('#txtGamma').val('0.198');

            //  
            $scope.pricing.txtOptionValue = '1.348';
            $scope.pricing.txtDelta = '0.231';
            $scope.pricing.txtGamma = '0.198';
            break;
        case 'SGD_IDR':
            $('#txtOptionValue').val('0.348');
            $('#txtDelta').val('1.2');
            $('#txtGamma').val('0.198');

            $scope.pricing.txtOptionValue = '0.348';
            $scope.pricing.txtDelta = '1.2';
            $scope.pricing.txtGamma = '0.198';
            break;
        case 'INR_IDR':
            $('#txtOptionValue').val('-2.48');
            $('#txtDelta').val('0.345');
            $('#txtGamma').val('0.1');
            $('#txtOptionValue').closest('.form-group').addClass('has-error');

            $scope.pricing.txtOptionValue = '-2.48';
            $scope.pricing.txtDelta = '0.345';
            $scope.pricing.txtGamma = '0.1';

            break;
        }
    };


    //pricing set while change the currency pair
    $scope.setCurrecyPair = function () {
        commonFactory.getConversionRate($scope.selectCurrencyPair, function (err, result) {
            $scope.inputPrice = result.data;
        });
        console.log('currency pair method called');
    };


});